import { IonApp, IonPage, IonSplitPane } from '@ionic/react';
import { Capacitor } from '@capacitor/core';
import firebase from 'firebase';
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { Header } from './components/common/Header';
import { Menu } from './components/Menu/Menu';
import { firebaseConfig } from './config/config';
import './constants/Interfaces';
import { AppPage} from './interfaces/Interface';
import rootReducer from './rootReducer';
import RoutingComponent from './RoutingComponent';

firebase.initializeApp(firebaseConfig);
let store: any
if (process.env.NODE_ENV === 'development')
  store = createStore(rootReducer, compose(applyMiddleware(thunk), window.devToolsExtension
    ? window.devToolsExtension() : (f: any) => f));
else
  store = createStore(rootReducer, compose(applyMiddleware(thunk)));

let platform = Capacitor.platform || '';

function App() {
  const menuItems: AppPage[] = [{ title: "home", url: "cscscscs" }]
  return (
    <Provider store={store}>
      <Router>
        <IonApp>
          <IonSplitPane contentId="main">
            <Menu appPages={menuItems} />
            <Header />
            <IonPage id="main">
              <RoutingComponent platform={platform}/>
            </IonPage>
          </IonSplitPane>
        </IonApp>
      </Router>
    </Provider>
  );
}

export default App;
