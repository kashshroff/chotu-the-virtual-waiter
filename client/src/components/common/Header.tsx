import { IonButtons, IonHeader, IonMenuButton, IonToolbar } from '@ionic/react';
import React from 'react';

export const Header: React.FC = () => {
    return (
        <IonHeader>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonMenuButton></IonMenuButton>
                </IonButtons>
            </IonToolbar>
        </IonHeader>
    )
}
