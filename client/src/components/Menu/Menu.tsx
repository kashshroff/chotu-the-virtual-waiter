import { IonContent, IonHeader, IonList, IonMenu, IonMenuToggle, IonTitle, IonToolbar } from '@ionic/react';
import * as React from 'react';
import { MenuProps, AppPage } from '../../interfaces/Interface';
import styles from './Menu.module.css'

export const MenuItems: React.FC<AppPage> = ({ title, url }) => {
    return (
        <IonMenuToggle key={title} autoHide={false}>
            <a className={styles.menu_items}>{title}</a>
        </IonMenuToggle>
    )
}

export const Menu: React.FC<MenuProps> = ({ appPages }) => {
    return (
        <IonMenu side="start" menuId="first" contentId='main'>
            <IonHeader>
                <IonToolbar >
                    <IonTitle>Oye Chottu</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonList className={styles.border}>
                    {appPages.map((page) => {
                        return <MenuItems {...page} />
                    })}
                </IonList>
            </IonContent>
        </IonMenu>
    )
}