export interface MenuProps{
    appPages: AppPage[]
}

export interface AppPage{
    title: string
    url: string
}