import { IonRouterOutlet } from '@ionic/react'
import React from 'react'
import { Route, Switch } from 'react-router-dom'
import HomePage from './components/homePage/HomePage'
import * as ROUTES from './constants/Routes'

const RoutingComponent: React.FC<{platform: string}> = ({platform})=>{
    return (
        <IonRouterOutlet>
            <Switch>
                {/* Add Routes here */}
                <Route exact path={ROUTES.HomePage} component={HomePage} />
            </Switch>
        </IonRouterOutlet>
    )
}
export default RoutingComponent
